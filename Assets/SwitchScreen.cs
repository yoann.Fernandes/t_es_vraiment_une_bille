﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScreen : MonoBehaviour
{
    public GameObject InitScreen;
    public GameObject GameScreen;
    // Start is called before the first frame update
    void Start()
    {
        GameScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            InitScreen.SetActive(false);
            GameScreen.SetActive(true);

        }
    }
}
