﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Direction : MonoBehaviour
{
    public int SpeedRotat = 50;
    public int Force = 1000;
    public int NbStepsMax = 5;
    public OpenCVForUnityExample.HandPoseEstimationExample Quad;
    public Text CoupsRestant;
    public GameObject FinDuNiveau;
    bool IsLvlFinish;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //DEKOMENTAIS CA KAN CA MARCHE : 
        //DEKOMENTAIS CA KAN CA MARCHE : 
        //((int)Quad.PtDetect.x-400
        //Input.GetAxis("Mouse X") 

        transform.Rotate(Vector3.up * ((int)Quad.PtDetect.x - 400) * Time.deltaTime * SpeedRotat);

        transform.position = GameObject.Find("Player").GetComponent<Transform>().position;
         if(GameObject.Find("Player").GetComponent<Rigidbody>().velocity.magnitude < 0.1){
            GameObject.Find("Direction Ligne").GetComponent<MeshRenderer>().enabled = true;   

            transform.Rotate(Vector3.up * ((int)Quad.PtDetect.x - 400) * Time.deltaTime * SpeedRotat);

            if (Quad.numberOfFingers == 0 && NbStepsMax > 0 ){
                GameObject.Find("Player").GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward)*Force);
                NbStepsMax--;
            }
        }

        if(GameObject.Find("Player").GetComponent<Rigidbody>().velocity.magnitude > 0.1){
            GameObject.Find("Direction Ligne").GetComponent<MeshRenderer>().enabled = false;   
        }

        if(NbStepsMax == 0)
        {
            Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
        }
        if (IsLvlFinish == false)
        {
            CoupsRestant.text = "Coups Réstant: " + NbStepsMax;
        }

    }
    private void OnCollisionEnter(GameObject FinDuNiveau)
    {
        IsLvlFinish = true;
        CoupsRestant.text = "GG GROSSE PETASSE";
    }
}
